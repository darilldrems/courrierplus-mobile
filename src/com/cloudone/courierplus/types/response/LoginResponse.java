package com.cloudone.courierplus.types.response;

import com.cloudone.courierplus.types.ApiError;
import com.cloudone.courierplus.types.User;

public class LoginResponse {
	private User user = null;
	private ApiError error = null;
	
	public void setUser(User u){
		this.user = u;
	}
	
	public void setError(ApiError r){
		this.error = r;
	}
	
	public User getUser(){
		return this.user;
	}
	
	public ApiError getError(){
		return this.error;
	}
}
