package com.cloudone.courierplus.types;

public class ApiError {
	private String success;
	private String message;
	
	
	public void setSuccess(String s){
		this.success = s;
	}
	
	public void setMessage(String m){
		this.message = m;
	}
	
	public String getSuccess(){
		return this.success;
	}
	
	public String getMessage(){
		return this.message;
	}
}
