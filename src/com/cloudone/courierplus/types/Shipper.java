package com.cloudone.courierplus.types;

public class Shipper {

	private String id = null;
	private String name = null;
	
	public Shipper(){
		
	}
	
	public String getId(){
		return this.id;
	}
	
	public String getName(){
		return this.name;
	}
	
	public void setId(String id){
		this.id = id;
	}
	
	public void setName(String name){
		this.name = name;
	}
}
