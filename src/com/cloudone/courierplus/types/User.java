package com.cloudone.courierplus.types;

public class User {
	private String firstName = null;
	private String lastName = null;
	private String username = null;
	private String email = null;
	private String role = null;
	private String createdOn = null;
	private String state = null;
	private String id = null;
	
	public String getId(){
		return this.id;
	}
	
	public void setId(String id){
		this.id = id;
	}
	
	public String getFirstName(){
		return this.firstName;
	}
	
	public String getLastName(){
		return this.lastName;
	}
	
	public String getUsername(){
		return this.username;
	}
	
	public String getEmail(){
		return this.email;
	}
	
	public String getRole(){
		return this.role;
	}
	
	public String getCreatedOn(){
		return this.createdOn;
	}
	
	public String getState(){
		return this.state;
	}
	
	public void setFirstName(String val){
		this.firstName = val;
	}
	
	public void setLastName(String val){
		this.lastName = val;
	}
	
	public void setUsername(String username){
		this.username = username;
	}
	
	public void setEmail(String email){
		this.email = email;
	}
	
	public void setRole(String role){
		this.role = role;
	}
	
	public void setCreatedOn(String c){
		this.createdOn = c;
	}
	
	public void setState(String st){
		this.state = st;
	}
	
	@Override
	public String toString(){
		return this.username + this.role;
		
	}
	
	
	
}
