package com.cloudone.courierplus.exceptions;

public class InvalidLoginDetails extends Exception{
	
	public InvalidLoginDetails(){
		
	}
	
	public InvalidLoginDetails(String msg){
		super(msg);
	}
}
