package com.cloudone.courierplus.exceptions;

public class SessionExpired extends Exception {
	
	public SessionExpired(){
		
	}
	
	public SessionExpired(String msg){
		super(msg);
	}
}
