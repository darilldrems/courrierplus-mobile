package com.cloudone.courierplus.exceptions;

public class UnExpectedError extends Exception{
	
	public UnExpectedError(){
		
	}
	
	public UnExpectedError(String s){
		super(s);
	}
}
