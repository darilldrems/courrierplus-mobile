package com.cloudone.courierplus.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.cloudone.courierplus.exceptions.InvalidLoginDetails;
import com.cloudone.courierplus.types.Shipper;
import com.cloudone.courierplus.types.User;
import com.cloudone.courierplus.types.response.LoginResponse;



//import org.apache.http.client.HttpClient;

import com.google.gson.*;

public class Client {
	HttpClient httpClient = new DefaultHttpClient();
	Routes routes = new Routes();
	
	HttpPost post;
	HttpGet get;
	HttpResponse response;
	
	
	public ArrayList<Shipper> downloadShippers(){
		ArrayList<Shipper> shippers = null;
		
		get = new HttpGet(routes.getShippersPath());		
		
		try {
			response = httpClient.execute(get);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return shippers;
		
	}
	
	public User login (String username, String password) throws InvalidLoginDetails{
		LoginResponse apiResponse = null;
		User user = null;
		post = new HttpPost(routes.getLoginPath());
		
		try{
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			
				nameValuePairs.add(new BasicNameValuePair("username", username));
				nameValuePairs.add(new BasicNameValuePair("password", password));
			
				post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			
				response = httpClient.execute(post);
				
				String responseInString = EntityUtils.toString(response.getEntity());
				System.out.println("response here:"+responseInString);
				
				if(responseInString.contains("error")){
					throw new InvalidLoginDetails();
				}
				
				Gson gson = new Gson();
				user = gson.fromJson(responseInString, User.class);
//				apiResponse = gson.fromJson(responseInString, LoginResponse.class);
				
				if(user == null){
//					System.out.println("its null in user");
					throw new InvalidLoginDetails();
				}
				
//				if(apiResponse.getError() == null){
//					System.out.println("its null in error");
//				}
				
				
//				System.out.print("user in apiResponse:"+apiResponse.getUser());
//				System.out.print("error:"+apiResponse.getError());
				
//				return user;
			
		} catch(ClientProtocolException e){
//			TODO: throw some exception
			throw new InvalidLoginDetails();
			
		} catch(IOException e){
//			TODO: throw some exception
			throw new InvalidLoginDetails();
		}
		
		
		
		return user;
	}
}
