package com.cloudone.courierplus.service;

import java.util.Arrays;

import com.cloudone.courierplus.exceptions.SessionExpired;
import com.cloudone.courierplus.types.User;

import android.content.Context;
import android.content.SharedPreferences;

public class Authentication {
	private final SharedPreferences loginPreferences;
	private final SharedPreferences.Editor editor;
	private final Context context;
	
	public Authentication(Context context){
		this.context = context;
		this.loginPreferences = context.getSharedPreferences("UserDetails", Context.MODE_PRIVATE);
		this.editor = this.loginPreferences.edit();
	}
	
	public void login (User user) throws SessionExpired{
		if(user.getEmail().isEmpty() && user.getRole().isEmpty() && user.getId().isEmpty()){
			throw new SessionExpired();
		}else{
			editor.putString("user_id", user.getEmail());
			editor.putString("user_username", user.getUsername());
			editor.putString("user_role", user.getId());
			editor.putString("user_state", user.getState());
			editor.putString("user_first_name", user.getFirstName());
			editor.putString("user_last_name", user.getLastName());
			
//			Java date time to set the time now
//			editor.putString("set_time", "");
			editor.putLong("last_set_time", System.currentTimeMillis());
		}
	}
	
	public boolean isLoggedin(){
		if(isSessionExpired()){
			return false;
		}
		return true;
	}
	
	public void logout(){
//		TODO clear the preferences data
		editor.putString("user_id", "");
		editor.putString("user_username", "");
		editor.putString("user_role", "");
		editor.putString("user_state", "");
		editor.putString("user_first_name", "");
		editor.putString("user_last_name", "");
		
		editor.putLong("last_set_time", 0);
	}
	
	private boolean isSessionExpired(){
		
		long set_time = this.loginPreferences.getLong("last_set_time", 0);
		
		long diff = System.currentTimeMillis() - set_time;
		
//		600000 milliseconds is 10 mins
		if(diff < 600000){
			return true;
		}
		
		return false;
	}
	
	public boolean hasPermission(String[] permittedRoles, String userRole){
		
		if(Arrays.asList(permittedRoles).contains(userRole)){
			return true;
		}
		
		return false;
	}
	
	public String getId() throws SessionExpired{
		String userId = loginPreferences.getString("user_id", "");
		if(userId.isEmpty()){
			throw new SessionExpired();
		}
		
		return userId;
	}
	
	public void getLoginDetails(){
		
		
		
	}
}
