package com.cloudone.courrierplus.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class SealActivity extends Activity{
	
	Spinner locationSpinner;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_seal);
		
		locationSpinner = (Spinner)findViewById(R.id.locationSpinner);
		
		addSpinnerItems();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu){
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void onHomeAction(MenuItem view){
		Intent intent = new Intent(this, MenuActivity.class);
		startActivity(intent);
	}
	
	private void addSpinnerItems(){
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
		        R.array.location_array, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		locationSpinner.setAdapter(adapter);
		
		
		
		
	}

}
