package com.cloudone.courrierplus.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ProcessingActivity extends Activity{
	
	ListView processingListView;
	String[] items = {"SEAL - 101MOB", "SEAL - 10998MOB", "SEAL - 8776MOB"};
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_processing);
		
		selectView();
		
		setListViewAdapter();
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu){
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void onHomeAction(MenuItem view){
		Intent intent = new Intent(this, MenuActivity.class);
		startActivity(intent);
	}
	
	private void selectView(){
		processingListView = (ListView)findViewById(R.id.processingListView);
	}
	
	private void setListViewAdapter(){
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items);
		processingListView.setAdapter(adapter);
		
		processingListView.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				goToSealActivity();
			}
			
		});
		
	}
	
	public void goToSealActivity(){
		Intent intent = new Intent(this, SealActivity.class);
		startActivity(intent);
	}

}
