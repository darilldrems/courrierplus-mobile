package com.cloudone.courrierplus.activity;

import android.os.Bundle;
import android.os.Handler;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudone.courierplus.exceptions.InvalidLoginDetails;
import com.cloudone.courierplus.exceptions.SessionExpired;
import com.cloudone.courierplus.service.Authentication;
import com.cloudone.courierplus.service.Client;
import com.cloudone.courierplus.types.User;
//import com.novatia.leadtrader.activity.LoginActivity;

public class LoginActivity extends Activity {
	
	Button submitButton;
	EditText usernameView;
	EditText passwordView;
	
	String username;
	String password;
	
	Authentication auth;
	Handler handler;
	User user = null;
	
	ProgressDialog dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		selectView();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}
	
	private void selectView(){
		usernameView = (EditText)findViewById(R.id.usernameEditText);
		passwordView = (EditText)findViewById(R.id.passwordEditText);
		
		submitButton = (Button)findViewById(R.id.submitButton);
		
		auth = new Authentication(this);
		
		submitButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				goToMenuActivity();
				
				username = usernameView.getText().toString();
				password = passwordView.getText().toString();
				
				if(!username.isEmpty()  && !password.isEmpty()){
//					Client client = new Client();
					
//					Boolean resp = client.login(usernameView.getText().toString(), passwordView.getText().toString());
					
					dialog = ProgressDialog.show(LoginActivity.this, "", "Connecting...", true);
					handler = new Handler();
					Thread thread = new Thread(new Runnable(){

						@Override
						public void run() {
							// TODO Auto-generated method stub
//							user = null;
							try{
								Client client = new Client();
								user = client.login(username, password);
								
								handler.post(new Runnable(){

									@Override
									public void run() {
										// TODO Auto-generated method stub
										dialog.dismiss();
										try{
											auth.login(user);
											goToMenuActivity();
										}catch(SessionExpired e){
											displayMessage("Incorrect username or password.");
										}
									}
									
								});	
								
							}catch(InvalidLoginDetails e){
								Log.e("Exception", "Exception thrown");
								handler.post(new Runnable(){

									@Override
									public void run() {
										dialog.dismiss();
										// TODO Auto-generated method stub
										displayMessage("Incorrect username or password.");
									}
									
								});
							}
						}
						
					});
					
					thread.start();
					
					
				}
			}
			
		});
	}
	
	private void goToMenuActivity(){
		Intent intent = new Intent(this, MenuActivity.class);
		startActivity(intent);
	}
	
	private void displayMessage(String msg){
		
		Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
		
	}
	
	@Override
	public void onBackPressed(){
		
	}
	
	

}
