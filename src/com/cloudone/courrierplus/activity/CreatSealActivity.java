package com.cloudone.courrierplus.activity;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

public class CreatSealActivity extends Activity{
	
	ArrayList<String> seals = new ArrayList<String>();
	
	ArrayAdapter<String> adapter;
	
	EditText shipmentNumberEditText;
	Button addShipmentButton, createSealButton;
	ListView shipmentListView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_seal);
		
		selectView();
		
		setListViewContent();
		
		addEventListeners();
		
		
	}
	
	private void selectView(){
		shipmentNumberEditText = (EditText)findViewById(R.id.shipmentNumberEditText);
		addShipmentButton = (Button) findViewById(R.id.addShipmentButton);
		createSealButton = (Button) findViewById(R.id.createSealButton);
		shipmentListView = (ListView) findViewById(R.id.shipmentListView);
		
	}
	
	private void addEventListeners(){
		
		addShipmentButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				addShipment();
			}
			
		});
		
		createSealButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				goToSealNumberActivity();
				
			}
			
		});
	}
	
	private void setListViewContent(){
		 adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, seals);
		shipmentListView.setAdapter(adapter);
	}
	
	private void goToSealNumberActivity(){
		Intent intent = new Intent(this, SealNumberActivity.class);
		startActivity(intent);
	}
	
	private void addShipment(){
		seals.add(shipmentNumberEditText.getText().toString());
		adapter.notifyDataSetChanged();
		
		shipmentNumberEditText.setText("");
	}
	
	

}
