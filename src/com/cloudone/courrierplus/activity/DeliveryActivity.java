package com.cloudone.courrierplus.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class DeliveryActivity extends Activity{
	
	ListView deliveryListView;
	String[] items = {"123456789", "7764356355", "098765432", "", "134567890", "678954321"};
	
	Button scanButton, bulkScanOutButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_delivery);
		
		selectView();
		
		setListViewContent();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu){
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void onHomeAction(MenuItem view){
		Intent intent = new Intent(this, MenuActivity.class);
		startActivity(intent);
	}
	
	
	private void selectView(){
		
		deliveryListView = (ListView)findViewById(R.id.deliveryListView);
		scanButton = (Button)findViewById(R.id.scanButton);
		bulkScanOutButton = (Button) findViewById(R.id.bulkScanOutButton);
		
		scanButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				goToDeliveryScan();
			}
			
		});
		
		bulkScanOutButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				goToBulkScanOut();
			}
			
		});
		
		
	}
	
	private void goToDeliveryScan(){
		Intent intent = new Intent(this, DeliveryScanActivity.class);
		startActivity(intent);
	}
	
	private void goToBulkScanOut(){
		Intent intent = new Intent(this, BulkScanOutActivity.class);
		startActivity(intent);
	}
	
	private void setListViewContent(){
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items);
		deliveryListView.setAdapter(adapter);
		
		deliveryListView.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				goToDeliveryDetail();
			}
			
		});
	}
	
	public void goToDeliveryDetail(){
		Intent intent = new Intent(this, DeliveryDetailActivity.class);
		startActivity(intent);
	}
}
