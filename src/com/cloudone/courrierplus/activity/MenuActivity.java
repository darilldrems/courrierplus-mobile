package com.cloudone.courrierplus.activity;

import com.cloudone.courierplus.service.Authentication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MenuActivity extends Activity implements OnClickListener{
	
	Button pickUpButton;
	Button deliveryButton;
	Button processingButton;
	Button createSealButton;
	
	Authentication auth;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu);
		
		selectView();
	}
	
	private void selectView(){
		pickUpButton = (Button) findViewById(R.id.pickUpButton);
		deliveryButton = (Button) findViewById(R.id.deliveryButton);
		processingButton = (Button) findViewById(R.id.processing);
		createSealButton = (Button) findViewById(R.id.createSealButton);
		
		pickUpButton.setOnClickListener(this);
		deliveryButton.setOnClickListener(this);
		processingButton.setOnClickListener(this);
		createSealButton.setOnClickListener(this);
		
		auth = new Authentication(this);
	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		
		if(auth.isLoggedin()){
			if(view.getId() == R.id.pickUpButton){
				Intent i = new Intent(this, PickUpActivity.class);
				startActivity(i);
				
			}else if(view.getId() == R.id.deliveryButton){
				Intent i = new Intent(this, DeliveryActivity.class);
				startActivity(i);
				
			}else if(view.getId() == R.id.processing){
				Intent i = new Intent(this, ProcessingActivity.class);
				startActivity(i);
				
			}else if(view.getId() == R.id.createSealButton){
				Intent intent = new Intent(this, CreatSealActivity.class);
				startActivity(intent);
			}else{
				
			}
		}else{
			logoutAndGoToLoginActivity();
		}
		
		
		
		
		
	}
	
	@Override
	public void onBackPressed(){
		
	}
	
	private void logoutAndGoToLoginActivity(){
		auth.logout();
		Intent intent = new Intent(this, LoginActivity.class);
		startActivity(intent);
	}

}
