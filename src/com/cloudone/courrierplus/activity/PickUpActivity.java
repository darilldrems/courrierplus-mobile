package com.cloudone.courrierplus.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class PickUpActivity extends Activity{
	
	Spinner paymentModeSpinner;
	Spinner contentSpinner, shipperSpinner;
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pickup);
		
		selectView();
		
		addSpinnerItems();
		
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu){
		getMenuInflater().inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	
	
	private void selectView(){
		paymentModeSpinner = (Spinner) findViewById(R.id.paymentModeSpinner);
		
		contentSpinner = (Spinner) findViewById(R.id.contentSpinner);
		
		shipperSpinner = (Spinner) findViewById(R.id.shipperSpinner);
	}
	
//	public void onHomeAction(MenuItem view){
//		Intent intent = new Intent(this, MenuActivity.class);
//		startActivity(intent);
//	}
	
	
	
	private void addSpinnerItems(){
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
		        R.array.payment_mode, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		paymentModeSpinner.setAdapter(adapter);
		
		
		ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this, R.array.content_array, android.R.layout.simple_spinner_item);
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		contentSpinner.setAdapter(adapter2);
		
		ArrayAdapter<CharSequence> adapter3 = ArrayAdapter.createFromResource(this, R.array.shippers_array, android.R.layout.simple_spinner_item);
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		shipperSpinner.setAdapter(adapter3);
		
	}
	
	private void downloadShippers(){
		
	}

}
