package com.cloudone.courrierplus.activity;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

public class PODActivity extends Activity{
	
	Spinner paymentModeSpinner;
	
	TextView dateTime;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pod);
		
		selectView();
		
		setSpinnerDetails();
		
		setDate();
	}
	
	
	private void selectView(){
		paymentModeSpinner = (Spinner) findViewById(R.id.paymentModeSpinner);
		dateTime = (TextView) findViewById(R.id.dateTime);
		
	}
	
	private void setDate(){
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:MM");
		Date date = new Date();
		
		dateTime.setText(dateFormat.format(date));
	}
	
	private void setSpinnerDetails(){
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
		        R.array.payment_mode, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		paymentModeSpinner.setAdapter(adapter);
	}

}
