package com.cloudone.courrierplus.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class DeliveryDetailActivity extends Activity implements OnClickListener{
	
	Button podButton, statusButton, returnButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_delivery_detail);
		
		selectView();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu){
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void onHomeAction(MenuItem view){
		Intent intent = new Intent(this, MenuActivity.class);
		startActivity(intent);
	}
	
	private void selectView(){
		podButton = (Button)findViewById(R.id.podButton);
		statusButton = (Button)findViewById(R.id.statusButton);
		returnButton = (Button)findViewById(R.id.returnButton);
		
		podButton.setOnClickListener(this);
		statusButton.setOnClickListener(this);
		returnButton.setOnClickListener(this);
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
			
		case R.id.podButton:
			Intent intent = new Intent(this, PODActivity.class);
			startActivity(intent);
			break;
		case R.id.statusButton:
			Intent intent2 = new Intent(this, StatusActivity.class);
			startActivity(intent2);
			break;
		case R.id.returnButton:
			Intent intent3 = new Intent(this, DeliveryConfirmationActivity.class);
			startActivity(intent3);
			break;
		default:
			break;
		}
		
	}
	
	

}
