var userController = require('../lib/controllers/user');
var shipperController = require('../lib/controllers/shipper');
var truckController = require('../lib/controllers/truck');
var shipmentController = require('../lib/controllers/shipment');
var branchController = require('../lib/controllers/branch');
var warehouseController = require('../lib/controllers/warehouse');
var sealController = require('../lib/controllers/seal');
var controlShuttleController = require('../lib/controllers/controlshuttle');
var registerController = require('../lib/controllers/register');
var loginController = require('../lib/controllers/login');
var dashboardController = require('../lib/controllers/dashboard');
var historyController = require('../lib/controllers/history');

var options = require('../lib/controllers/options');

var authentication = require('../lib/helpers/authentication');


var searchController = require('../lib/controllers/search');


module.exports = function(app){
    app.get('/', function(req, res){
//        console.log('in here');
        res.render('index.html', {});
    });


    app.get('/backend/api/options/shipper', authentication.loginRequired, options.shipper_list);
    app.get('/backend/api/options/status', authentication.loginRequired, options.delivery_status);
    app.get('/backend/api/options/content', authentication.loginRequired, options.content_types);

//    dashboard route
    app.get('/backend/api/dashboard', authentication.loginRequired, dashboardController);

//    History routes
    app.get('/backend/api/history/shipment', authentication.loginRequired, historyController.shipment_history_list);
    app.get('/backend/api/history/seal', authentication.loginRequired, historyController.seal_history_list);
    app.get('/backend/api/history/shuttle', authentication.loginRequired, historyController.shuttle_history_list);



//    search route
    app.post('/backend/api/search/:model', authentication.loginRequired, searchController.search);

//    app.get('/backend/api/show', loginController.showLoginUser);

//    logout route
    app.get('/backend/api/logout', loginController.logout);
    app.get('/logout', loginController.logout);

//    authentication
    app.post('/backend/api/register', registerController.register);
    app.post('/backend/api/login', loginController.login);
    app.get('/backend/api/l', authentication.loginRequired, registerController.listAuth)

//    user routes
    app.get('/backend/api/users', authentication.loginRequired, userController.listUsers);
//    app.post('/backend/api/user/new', userController.createUser);
    app.get('/backend/api/user/:id', authentication.loginRequired, userController.getUser);
    app.post('/backend/api/user/delete', authentication.loginRequired, userController.deleteUser);
    app.post('/backend/api/user/edit/:id', authentication.loginRequired, userController.editUser);



//    shipper routes
    app.post('/backend/api/shipper/new', authentication.loginRequired, shipperController.createShipper);
    app.get('/backend/api/shipper/:id', authentication.loginRequired, shipperController.getShipper);
    app.get('/backend/api/shippers', authentication.loginRequired, shipperController.listShippers);
    app.post('/backend/api/shipper/edit/:id', authentication.loginRequired, shipperController.editShipper);
    app.post('/backend/api/shipper/delete', authentication.loginRequired, shipperController.deleteShipper);

//    truck routes

    app.get('/backend/api/truck/:id', authentication.loginRequired, truckController.getTruck);
    app.post('/backend/api/truck/edit/:id', authentication.loginRequired, truckController.editTruck);
    app.post('/backend/api/truck/new', authentication.loginRequired, truckController.createTruck);
    app.get('/backend/api/trucks', authentication.loginRequired, truckController.listTrucks);
    app.post('/backend/api/truck/delete', authentication.loginRequired, truckController.deleteTruck);

//    shipment routes
    app.get('/backend/api/shipment/:id', authentication.loginRequired, shipmentController.getShipment);
    app.get('/backend/api/shipments', authentication.loginRequired, shipmentController.listShipments);
    app.post('/backend/api/shipment/new', authentication.loginRequired, shipmentController.createShipment);
    app.post('/backend/api/shipment/edit/:id', authentication.loginRequired, shipmentController.editShipment);
    app.post('/backend/api/shipment/delete', authentication.loginRequired, shipmentController.deleteShipment);


//    branch routes
    app.get('/backend/api/branch/:id', authentication.loginRequired, branchController.getBranch);
    app.get('/backend/api/branches', authentication.loginRequired, branchController.listBranches);
    app.post('/backend/api/branch/new', authentication.loginRequired, branchController.createBranch);
    app.post('/backend/api/branch/edit/:id', authentication.loginRequired, branchController.editBranch);
    app.post('/backend/api/branch/delete', authentication.loginRequired, branchController.deleteBranch);

//   warehouse routes
    app.get('/backend/api/warehouse/:id', authentication.loginRequired, warehouseController.getWarehouse);
    app.get('/backend/api/warehouses', authentication.loginRequired, warehouseController.listWarehouses);
    app.post('/backend/api/warehouse/new', authentication.loginRequired, warehouseController.createWarehouse);
    app.post('/backend/api/warehouse/edit/:id', authentication.loginRequired, warehouseController.editWarehouse);
    app.post('/backend/api/warehouse/delete', authentication.loginRequired, warehouseController.deleteWarehouse);


    //   seal routes
    app.get('/backend/api/seal/:id', authentication.loginRequired, sealController.getSeal);
    app.get('/backend/api/seals', authentication.loginRequired, sealController.listSeals);
    app.post('/backend/api/seal/new', authentication.loginRequired, sealController.createSeal);
    app.post('/backend/api/seal/edit/:id', authentication.loginRequired, sealController.editSeal);
    app.post('/backend/api/seal/delete', authentication.loginRequired, sealController.deleteSeal);


    //   controlshuttle routes
    app.get('/backend/api/cs/:id', authentication.loginRequired, controlShuttleController.getControlShuttle);
    app.get('/backend/api/cs', authentication.loginRequired, controlShuttleController.listControlShuttles);
    app.post('/backend/api/cs/new', authentication.loginRequired, controlShuttleController.createControlShuttle);
    app.post('/backend/api/cs/edit/:id', authentication.loginRequired, controlShuttleController.editControlShuttle);
    app.post('/backend/api/cs/delete', authentication.loginRequired, controlShuttleController.deleteControlShuttle);


    var mobile_login = require('../lib/controllers/mobile/login');
    var download = require('../lib/controllers/mobile/download');
    var pickup = require('../lib/controllers/mobile/pickup');
    var shipment = require('../lib/controllers/mobile/update-status');
    var seal = require('../lib/controllers/mobile/new-seal');
    var seal_location = require('../lib/controllers/mobile/update-seal-location');
    var data = require('../lib/controllers/mobile/data');
//    mobile api url
    app.post('/backend/mobile/api/login', mobile_login.login);
    app.post('/backend/mobile/api/download', download.download);
    app.post('/backend/mobile/api/pickup', pickup.pickup);
    app.post('/backend/mobile/api/shipment/update', shipment.update);
    app.post('/backend/mobile/api/seal/new', seal.seal);
    app.post('/backend/mobile/api/seal/location', seal_location.location);
    app.get('/backend/mobile/api/data/shippers', data.shippers);
    app.get('/backend/mobile/api/data/location', data.locations);




}