
/**
 * Module dependencies.
 */

var express = require('express');

//var user = require('./routes/user');
var http = require('http');
var path = require('path');
var mongoose = require('mongoose');
var app = express();


// all environments
app.set('port', process.env.PORT || 3000);
app.set('view engine', 'html'); // set up html for templating
app.engine('.html', require('ejs').__express);
app.set('views', __dirname + '/views');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser('S3CRE7'));
app.use(express.cookieSession());

//make sure every route request passes through login required before granting access to the data
//app.use(require('./lib/helpers/authentication').loginRequired);

app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));



mongoose.connect('mongodb://localhost/test');

mongoose.connection.on('connected', function(){
    console.log('connected to mongo test db');
});

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
  app.locals.pretty = true;
}

require('./routes/url')(app);


http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
