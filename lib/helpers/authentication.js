var Auth = require('../models/auth');
var User = require('../models/user');

login_path = '/'

exports.loginRequired = function(req, res, next){
    console.log('path is '+req.path);
    console.log();
    if( isLoggedin(req)){
//        console.log("in here next wit user "+req.session.user);
        var truthy = req.path.indexOf('/backend') == -1;
        console.log('login status'+ truthy);
        console.log(req.path);
        console.log('going to next');
        next();
    }else{

        return res.status(401).json({error: "Un authorized user"});
    }
}

var isLoggedin = exports.isLoggedin = function(req){
    return req.session.user != null;
}

exports.login = function(req, user){
    req.session.user = user;
}

exports.logout = function(req){
    req.session.user = null;
}


exports.userWithIdExist = function(id, callback){
    User.findById(id, function(err, u){
        if(err || u == null || u == undefined){
            callback({
                success: false,
                message: 'Incorrect username or password'
            });
        }else{
            Auth.findOne({user: u._id}, function(err, au){
                if(err || au == undefined || au == null){
                    callback({
                        success: false,
                        message: 'Incorrect username or password'
                    });
                }else{
                    callback({
                        success: true,
                        user: u
                    })
                }
            })

        }
    })
}

exports.userExist = function(name, pass, callback){
    User.findOne({username: name}, function(err, u){
        if(err || u == null || u == undefined){
            callback({
                success: false,
                message: 'Incorrect username or password'
            });
        }else{
            Auth.findOne({user: u._id, password: pass}, function(err, au){
                if(err || au == undefined || au == null){
                    callback({
                        success: false,
                        message: 'Incorrect username or password'
                    });
                }else{
                    callback({
                        success: true,
                        user: u
                    })
                }
            })

        }
    })

}