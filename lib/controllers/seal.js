var Seal = require('../models/seal');
var SealHistory = require('../models/seal_history');
var errorHelper = require('mongoose-error-helper').errorHelper;
var _underscore = require('underscore');

require('datejs');
exports.createSeal = function(req, res){
//    var n = [];

//    _underscore.forEach(req.body.shipments, function(a){
//        console.log(a);
//    })

//    return res.send('done');
    var seal = new Seal(req.body);
    seal.save(function(error, se){
        if(error){
            console.log('error occured: '+error);
            return res.json({error: errorHelper(error)});
        }else{
            console.log('created: '+se);
            return res.json({seal: se});
        }
    });
}

exports.getSeal = function(req, res){
    Seal.findById(req.params.id, function(error, seal){
        if(error){
            return res.json({error: errorHelper(error)});
        }else{
            return res.json({seal: seal});
        }
    });
}

exports.editSeal = function(req, res){

    Seal.findById(req.params.id, function(err, obj){
        if(!err){
            var seal_history = new SealHistory({edited_by: req.session.user._id});
            seal_history.addOldValues(obj);
            Seal.findByIdAndUpdate(req.params.id, {$set: req.body}, function(error, seal){
                if(error){
                    return res.json({error: errorHelper(error)});
                }else{
                    seal_history.addNewValues(seal)
                    var history_promise = seal_history.saveQ();

                    history_promise.then(function(s){
                        return res.json({seal: seal});
                    })

                }
            });
        }else{
            return res.json({error: err});
        }
    })

}

exports.deleteSeal = function(req, res){
    Seal.findByIdAndUpdate(req.params.id, {$set: {state: 'deactivated'}}, function(error, seal){
        if(error){
            return res.json({error: errorHelper(error)});
        }else{
            return res.json({seal: seal});
        }
    });
}

exports.listSeals = function(req, res){
    var page_count = 1;
    var post_per_page = 20;

    var condition = {
        state: 'active'
    };

    if(req.query.number)
        condition.number = req.query.number

    if(req.query.date)
        condition.date = {$gt:Date.parse(req.query.date)}

    if(req.params.page){
        page_count = parseInt(req.params.page);
//            console.log(req.params.page);
    }

    Seal
        .paginate(condition, page_count, post_per_page, function(error, pageCount, results, itemCount){
            if(error){
                return res.json({error: errorHelper(error)});
            }else{
                return res.json({seals: results, pagecount: pageCount, itemcount: itemCount});
            }
        });
}

