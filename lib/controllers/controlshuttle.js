var ControlShuttle = require('../models/controlshuttle');
var ShuttleHistory = require('../models/shuttle_history');
var errorHelper = require('mongoose-error-helper').errorHelper;

require('datejs');

exports.createControlShuttle = function(req, res){
    var controlShuttle = new ControlShuttle(req.body);
    controlShuttle.save(function(error, cs){
        if(error){
            return res.json({error: errorHelper(error)});
        }else{
            return res.json({cs: cs});
        }
    });
}

exports.getControlShuttle = function(req, res){
    ControlShuttle.findById(req.params.id, function(error, cs){
        if(error){
            return res.json({error: errorHelper(error)});
        }else{
            return res.json({cs: cs});
        }
    });
}

exports.editControlShuttle = function(req, res){

    ControlShuttle.findById(req.params.id, function(err, obj){
        if(err){
            return res.json({error: err});
        }else{
            var shuttleHistory = new ShuttleHistory({edited_by: req.session.user._id});
            shuttleHistory.addOldValues(obj);

            ControlShuttle.findByIdAndUpdate(req.params.id, {$set: req.body}, function(error, cs){
                if(error){
                    return res.json({error: errorHelper(error)});
                }else{
                    shuttleHistory.addNewValues(cs);
                    shuttleHistory.saveQ().then(function(r){
                        return res.json({cs: cs});
                    })

                }
            });

        }
    })



}

exports.deleteControlShuttle = function(req, res){
    ControlShuttle.findByIdAndUpdate(req.params.id, {$set: {state: 'deactivated'}}, function(error, cs){
        if(error){
            return res.json({error: errorHelper(error)});
        }else{
            return res.json({cs: cs});
        }
    });
}

exports.listControlShuttles = function(req, res){
    var page_count = 1;
    var post_per_page = 20;

    var condition = {
        state: 'active'
    }

    if(req.params.page){
        page_count = parseInt(req.params.page);
//            console.log(req.params.page);
    }

    if(req.query.date)
        condition.date = {$gt: Date.parse(req.query.date)};

    if(req.query.number)
        condition.number = req.query.number;



    ControlShuttle
        .paginate(condition, page_count, post_per_page, function(error, pageCount, results, itemCount){
            if(error){
                return res.json({error: errorHelper(error)});
            }else{
                return res.json({cs: results, pagecount: pageCount, itemcount: itemCount});
            }
        });
}

