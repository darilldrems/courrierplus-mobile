var md5 = require('MD5');
var authentication = require('../../helpers/authentication');
var Seal = require('../../models/seal');

exports.location = function(req, res){

    var user_id = req.body.user_id;

    authentication.userWithIdExist(user_id, function(result){
        if(result.success){
            Seal.findOne({number: req.body.number}, function(error, seal){
                if(error){
                    return res.json({error:{message: 'Unable to find seal. Check number and try again later'}});
                }else{
                    seal.location = req.body.location;
                    seal.save(function(error, saved_seal){
                        if(error){
                           return res.json({error: {message: 'Some errors occurred. Try again later'}});
                        }else{
                            return res.json(saved_seal);
                        }
                    })
                }
            })
        }else{
            return res.json({error:{message: 'Unauthorized user'}});
        }
    })
}