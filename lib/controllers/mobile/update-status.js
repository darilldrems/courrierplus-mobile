var Shipment = require('../../models/shipment');
var md5 = require('MD5');
var authentication = require('../../helpers/authentication');

exports.update = function(req, res){
    var user_id = req.body.user_id;


    authentication.userWithIdExist(user_id, function(result){
        if(result.success){
            var shipment_hawb = req.body.hawb;

            Shipment.findOne({hawb: shipment_hawb}, function(error, s){
                if(error){
                    return res.json({error: {message: 'Could not find shipment. Please check the hawb again.'}});
                }else{
                    var shipment = s;

                    shipment.status = req.body.status;
                    shipment.save(function(error, shipm){
                        if(error){
                            return res.json({error: {message: 'Could not change shipment status. Please try again later'}});
                        }else{
                            return res.json(shipm);
                        }
                    })
                }
            })
        }else{
            return res.json({error: {message: 'Unauthorized user'}});
        }


    })
}