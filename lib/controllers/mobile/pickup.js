var authentication = require('../../helpers/authentication');
var md5 = require('MD5');
var Shipment = require('../../models/shipment');

exports.pickup = function(req, res){
    var user_id = req.body.user_id;

    authentication.userWithIdExist(user_id, function(result){
        if(result.success){
            var new_pickup = new Shipment();

            new_pickup.shipper = req.body.shipper;
            new_pickup.content = req.body.content;

//            TODO:set status to new pickup
            new_pickup.status = 'Picked Up';

            new_pickup.consignee_name = req.body.consignee_name;
            new_pickup.consignee_tel = req.body.consignee_tel;
            new_pickup.consignee_address = req.body.consignee_address;
            new_pickup.remark = req.body.remark;
            new_pickup.payment_mode = req.body.payment_mode;
            new_pickup.pick_up_by = req.body.user_id;
            new_pickup.weight = req.body.weight;

//            TODO: send to orbitrax to generate HAWB then add to database and respond with details and hawb

            new_pickup.hawb = req.body.hawb;

//            get new pickup and generate a hawb from orbitrax.
            new_pickup.save(function(err, saved){
                if(err){
                    console.log(err);
                    return res.json({error: {message: 'Pickup was not created. Please try again later'}});
                }else{
                    saved.id = saved._id;
                    return res.json(saved)
                }
            })



        }else{
            return res.json({error:{message: 'permission denied'}})
        }
    })
}
