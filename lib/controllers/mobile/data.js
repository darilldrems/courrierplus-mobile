var Shipper = require('../../models/shipper');

exports.shippers = function(req, res){
    Shipper.find({}, function(error, results){
        if(error){
            return res.json({error: {message: "some errors occured"}});
        }else{
            var shippers = [];
            for(var i = 0; i < results.length; i++){
                var shipper = {
                    id: results[i]._id,
                    name: results[i].name
                };
//                shipper.id = results[i]._id;
                shippers.push(shipper);
            }
            return res.json(shippers);
        }
    })
}

exports.locations = function(req, res){
    return res.json(['Abuja', 'Lagos', 'Lokoja', 'Benue', 'Ibadan']);
}