var Shipment = require('../models/shipment');
var Seal = require('../models/seal');
require('datejs');


module.exports = function(req, res){

    var new_pickups = null;
    var new_seals = null;
    var delivered_shipments = null;
    var out_for_delivery = null;
    var failed_deliveries = null;

    var shipments_by_status = null;
    var recent_deliveries = null;

    var today = Date.today().addDays(-1).set({ hour: 1, minute: 0 });
    console.log(today > Date.today().set({hour:21, minute:41}));

//    today.setHours(0,0,0,0);

    Shipment.count({status: 'Picked Up', state: 'active', pick_up_time:{$gt:today}}, function(er, c){
        new_pickups = c
//        console.log("c is "+c);

        Shipment.count({status: 'Delivered', state:'active', delivery_time: {$gt:today}}, function(er, c){
            delivered_shipments = c;

            Shipment.count({status: 'Out for delivery', state: 'active'}, function(er, c){
                out_for_delivery = c

                Seal.count({state: 'active', created_on: {$gt: today}}, function(er, c){
                    new_seals = c;

                    Shipment.aggregate(
                        { $group:
                        { _id: '$status', total: { $sum: 1 } }
                        },
                        function (err, result) {

                            shipments_by_status = result;

                            Shipment.find({state: 'active',
                                status: { $not : {$in : ['Delivered', 'Out for delivery', 'picked up', 'Picked Up']}}}, function(err, result){
                                failed_deliveries = result;

                                Shipment.find({state: 'active', status: 'Delivered', delivery_time: {$gt: (30).minutes().ago()} },null, {sort:{delivery_time:-1}}, function(er, result){
                                    recent_deliveries = result;
                                    return res.json({
                                        today:{
                                            total_pickups: new_pickups,
                                            total_seals: new_seals,
                                            total_shipments_delivered: delivered_shipments,
                                            shipments_out_for_delivery: out_for_delivery
                                        },
                                        failed_deliveries: failed_deliveries,
                                        shipments_by_status: shipments_by_status,
                                        recent_deliveries: recent_deliveries
                                    })
                                })

                            });

                        }
                    );


                });


            });


        });

    });














    var d = (7).days().ago();
    console.log("7 days ago"+d);





}