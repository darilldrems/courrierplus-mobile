var authentication = require('../helpers/authentication');
var md5 = require('MD5');

exports.login = function(req, res){
    var username = req.body.username,
        password = md5(req.body.password);



    authentication.userExist(username, password, function(result){
        if(result.success){
            authentication.login(req, result.user);
            return res.json({user: result.user});
        }else{
            return res.json({error: result});
        }
    });

}


//exports.showLoginUser = function(req, res){
//    return res.json({data: req.session.user});
//}

exports.logout = function(req, res){
    delete req.session.user;
    return res.redirect('/#/login')
}