var Shipper = require('../models/shipper');
var mongoose = require('mongoose');
var errorHelper = require('mongoose-error-helper').errorHelper;

exports.createShipper = function(req, res){
//    var name = req.body.name,
//        address = req.body.address,
//        tel = req.body.tel,
//        type = req.body.type,
//        email = req.body.email;
//    return res.json({data: req.body});
    var shipper = new Shipper(req.body);

    shipper.save(function(error, sh){
        if(error){
            return res.json({error: errorHelper(error)});
        }else{
            return res.json({shipper: sh});
        }
    });

}

exports.getShipper = function(req, res){
//    return res.json({data: mongoose.Schema.Types.ObjectId(req.params.id)});
    Shipper.findOne({_id: req.params.id}, function(err, sh){
        if(err){
            return res.json({error: errorHelper(err)});
        }else{
            res.json({shipper: sh});
        }
    });
}

exports.listShippers = function(req, res){
    var page = 1;
    var total_per_page = 20;
    var condition = {
        state: 'active'
    }

    if(req.query.page)
        page = req.query.page

    if(req.query.name)
        condition.name = req.query.name


    Shipper.paginate(condition, page, total_per_page, function(err, pageCount, results, itemCount){
        if(err){
            return res.json({error: errorHelper(err)});
        }
        else{
            return res.json({shippers: results, pagecount: pageCount, itemcount: itemCount});
        }
    }, {sortBy: {createdOn: -1}});

}

exports.editShipper = function(req, res){
    Shipper.findByIdAndUpdate(req.params.id, {$set: req.body}, function(err, sh){
        if(err){
            return res.json({error: errorHelper(err)});
        }else{
            return res.json({shipper: sh});
        }
    });
}

exports.deleteShipper = function(req, res){

    Shipper.findById(req.body.id, function(err, sh){
       if(err){
           return res.json({error:err});
       }else{
          sh.state = 'deactivated';
           sh.save(function(error, shipper){
               if(!error){
                   return res.json({shipper: shipper});
               }else{
                   return res.json({error: errorHelper(error)});
               }
           });
       }
    });
}