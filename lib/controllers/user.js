var mongoose = require('mongoose');
var User = require('../models/user');
var underscore = require('underscore');
var errorHelper = require('mongoose-error-helper').errorHelper



//exports.createUser = function(req, res, next){
//
//
//        var first_name = req.body.first_name,
//            last_name = req.body.last_name,
//            email = req.body.email,
//            username = req.body.username,
//            role = req.body.role,
//            state = req.body.state;
//
//
//        if(first_name && last_name && email && username && role){
//            var new_user = new User({
//               first_name: first_name,
//               last_name: last_name,
//               username: username,
//               email: email,
//                role: role,
//                state: state
//            });
//
//            new_user.save(function(_error, _user){
//                if(!_error){
//                    return res.json({user: _user});
//                }else{
////                    console.log(_err);
//                    return res.json({error: _error});
//                }
//            });
//        }
//
//
//};


exports.listUsers = function(req, res, next){

//        return res.send(req.path);

        var page_count = 1;
        var post_per_page = 20;

        if(req.params.page){
            page_count = parseInt(req.params.page);
//            console.log(req.params.page);
        }

        User
            .paginate({state: 'active'}, page_count, post_per_page, function(error, pageCount, results, itemCount){
                if(error){
                    return res.json({error: errorHelper(error)});
                }else{
                    return res.json({users: results, pagecount: pageCount, itemcount: itemCount});
                }
            }, {sortBy : { created_on : -1 }});





}

exports.deleteUser = function(req, res, next){
    var id = req.body.id;
//    console.log(id);

    User.findOne({_id: id}, function(err, user){
        if(err){
            return res.json({error: errorHelper(err)});
        }else{
            user.state = 'deactivated';
            user.save(function(err, _us){
                if(err){
                    return res.json({error: errorHelper(err)});
                }else{
                    return res.json({user: _us});
                }
            });
        }
    });

}

exports.editUser = function(req, res){
    var id = req.params.id;

    User.findByIdAndUpdate(id, {$set: req.body}, function(error, user){
       if(error){
           return res.json({error:error});
       }else{
            return res.json({user: user});
       }
    });

}

exports.getUser = function(req, res){
    var id = req.params.id;
//    return res.json({username: username});

    User.findOne({_id: id}, function(err, user){
        if(err){
            res.json({error: err});
        }else{
            res.json({user: user});
        }
    });

}