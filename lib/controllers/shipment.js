var Shipment = require('../models/shipment');
var ShipmentHistory = require('../models/shipment_history.js');
var errorHelper = require('mongoose-error-helper').errorHelper;

require('datejs');

exports.createShipment = function(req, res){
    var shipment = new Shipment(req.body);
    shipment.pick_up_by = req.session.user._id;
    shipment.save(function(error, ship){
        if(error){
            return res.json({error: errorHelper(error)});
        }else{
            return res.json({shipment: ship});
        }
    });
}

exports.getShipment = function(req, res){
    Shipment.findOne({hawb: req.params.id}).populate('shipper pick_up_by delivered_by warehouse').exec(function(error, sh){
        if(error){
            return res.json({error: errorHelper(error)});
        }else{
            return res.json({shipment: sh});
        }
    });
}

exports.editShipment = function(req, res){
    if(String(req.body.status) === 'Delivered'){
        req.body.delivery_time = Date.now();
    }
    console.log('hawb id:'+req.params.id);
    Shipment.findById(req.params.id, function(err, obj){
        if(err){
            return res.json({error: err})
        }else{

            var shipmentHistory = new ShipmentHistory({edited_by: req.session.user._id});
            shipmentHistory.addOldValues(obj);

            Shipment.findByIdAndUpdate(req.params.id, {$set: req.body}, function(error, shi){
                if(error){
                    return res.json({error: errorHelper(error)});
                }else{
                    shipmentHistory.addNewValues(shi);
                    console.log('shipment history:'+shipmentHistory);
                    var promise = shipmentHistory.saveQ();
                    promise.then(function(re){
                        if(re.error){
                            console.log(re.error);
                        }else{
                            return res.json({shipment: shi});
                        }

                    })

                }
            });


        }
    })




}

exports.deleteShipment = function(req, res){
    Shipment.findByIdAndUpdate(req.params.id, {$set: {state: 'deactivated'}}, function(error, shi){
        if(error){
            return res.json({error: errorHelper(error)});
        }else{
            return res.json({shipment: shi});
        }
    });
}

exports.listShipments = function(req, res){
    var page_count = 1;
    var post_per_page = 20;

    console.log('list path with conditions: '+req.query.date);

    if(req.params.page){
        page_count = parseInt(req.params.page);
//            console.log(req.params.page);
    }

    var condition = {
        state: 'active'
    }

    if(req.query.hawb)
        condition.hawb = req.query.hawb;
    if(req.query.status)
        condition.status = req.query.status;
    if(req.query.content)
        condition.content = req.query.content;
    if(req.query.shipper)
        condition['shipper.name'] = req.query.shipper;
    if(req.query.date)
        condition.pick_up_time = {$gt:Date.parse(req.query.date)};

    Shipment
        .paginate(condition, page_count, post_per_page, function(error, pageCount, results, itemCount){
            if(error){
                return res.json({error: errorHelper(error)});
            }else{
                return res.json({shipments: results, pagecount: pageCount, itemcount: itemCount});
            }
        }, {populate: 'warehouse shipper', sortBy : { pick_up_time : -1 }});
}