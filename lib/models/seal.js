var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

var sealSchema = new mongoose.Schema({
    number: {type: String, required: true, unique: true, lowercase:true},
    departure: {type: String, required: true},
    destination: {type: String, required: true},
    shipments: [],
    location:{type: String, required: true},
    created_on: {type: Date, default: Date.now()},
    updated_on: Date,
    state: {type: String, enum: ['active', 'deactivated'], default:'active'}
});

sealSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Seal', sealSchema);