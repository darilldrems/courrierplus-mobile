var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

status_enum = ['Picked Up', 'Out for delivery', 'Delivered', 'Consignee moved'];

var shipmentSchema = new mongoose.Schema({
    hawb: {type: String, required: true, unique: true, lowercase:true},
    shipper: {type: mongoose.Schema.Types.ObjectId, ref:'Shipper', required: true},
    content: {type: String, enum:['Document', 'Non Document']},
    status : {type: String, required: true, enum: status_enum},
    consignee_name: {type: String, required: true},
    consignee_tel: {type: String, required: true},
    consignee_address: {type: String, required: true},
    weight: Number,
    amount: Number,
    payment_mode: {type: String, enum:['Cash on delivery', 'POS']},
    remark: String,
    pick_up_by: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
    pick_up_time: {type: Date, default: Date.now()},
    deliverered_by: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    delivery_time: Date,
    warehouse: {type: mongoose.Schema.Types.ObjectId, ref: 'Warehouse'},
    last_modified: Date,
    state: {type: String, enum:['active', 'deactivated'], default:'active'}
});

shipmentSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Shipment', shipmentSchema);

