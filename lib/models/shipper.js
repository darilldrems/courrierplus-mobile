var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

var shipperSchema = new mongoose.Schema({
    name: {type: String, unique: true, required: true, lowercase:true},
    address: String,
    tel: String,
    type: {type: String, enum:['Credit', 'Non Credit']},
    email: {type: String, required: true, unique: true},
    state: {type: String, enum: ['active', 'deactivated'], default:'active'},
    createdOn: {type: Date, default: Date.now()}
});

shipperSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Shipper', shipperSchema);