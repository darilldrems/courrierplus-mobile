var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

user_roles = ['personel', 'administrator', 'control', ''];

var userSchema = new mongoose.Schema({
    first_name: String,
    last_name: String,
    username: {type: String, unique: true, required: true, lowercase:true},
    email: String,
    role: {type: String, enum: user_roles, default: 'personel'},
    created_on: {type: Date, default: Date.now()},
    state: {type: String, enum: ['active', 'deactivated'], default: 'active'}
});

userSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('User', userSchema);