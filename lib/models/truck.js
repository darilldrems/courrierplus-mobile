var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

var truckSchema = new mongoose.Schema({
    name: {type: String, required: true, unique: true, lowercase:true},
    identification: {type: String, required: true, unique: true, lowercase:true},
    location: String,
    state: {type: String, default: 'active', enum:['active', 'deactivated']}
});

truckSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Truck', truckSchema);