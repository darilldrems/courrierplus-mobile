var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

var warehouseSchema = new mongoose.Schema({
    name: {type: String, required: true, unique: true, lowercase:true},
    branch: {type: mongoose.Schema.Types.ObjectId, ref: 'Branch'},
    state: {type: String, enum:['active', 'deactivated'], default:'active'}
});

warehouseSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Warehouse', warehouseSchema);