var app = angular.module('courierPlusApp', ['ngRoute', 'courierPlusControllers', 'BackEndService', 'MyFilters', 'CustomDirectives', 'angularMoment']);

app.run(function($rootScope) {
    $rootScope.accessors = {
        getId: function(row) {
            return row._id
        }
    }
});

app.config(['$routeProvider', '$httpProvider', function($routeProvider, $httpProvider){
    $routeProvider.
        when('/login', {
            templateUrl: '/partials/login.html',
            controller: 'LoginController'

        }).
        when('/dashboard', {
            templateUrl: '/partials/dashboard.html',
            controller: 'DashboardController',
            resolve: {
                dashData: function($http){
                    return $http.get('backend/api/dashboard').then(function(res){
                        return res.data;
                    });
                }
            }
        }).
        when('/shipments', {
            templateUrl: 'partials/shipments.html',
            controller: 'ShipmentsController',
            resolve: {
                shipmentLists: function($http){
                    return $http.get('backend/api/shipments').then(function(res){
                       return res.data;
                    });

                },

                shippersOption: function($http){
                    return $http.get('backend/api/options/shipper').then(function(res){
                        return res.data;
                    })
                },

                statusOption: function($http){
                    return $http.get('backend/api/options/status').then(function(res){
                        return res.data;
                    })
                },

                contentOption: function($http){
                    return $http.get('backend/api/options/content').then(function(res){
                        return res.data;
                    })
                }

            }
        }).
        when('/shipment/new', {
            templateUrl: '/partials/shipment-new.html',
            controller: 'ShipmentNewController',
            resolve:{
                shippersList: function($http){
                    return $http.get('backend/api/shippers').then(function(res){
                       return res.data;
                    });
                },
                warehouseList: function($http){
                    return $http.get('backend/api/warehouses').then(function(res){
                        return res.data;
                    });
                }

            }
        }).
        when('/shipment/detail/:hawb', {
            templateUrl: '/partials/shipment-detail.html',
            controller: 'ShipmentDetailController'
        }).
        when('/shipment/edit/:id', {
            templateUrl: '/partials/shipment-edit.html',
            controller: 'ShipmentEditController',
            resolve: {
                shipment: function($route, $http){
                    return $http.get('backend/api/shipment/'+$route.current.params.id).then(function(res){
                       return res.data;
                    });
                }
            }
        }).
        when('/shippers', {
           templateUrl: '/partials/shippers.html',
           controller: 'ShippersController',
            resolve: {
                shippersList: function($http){
                    return $http.get('backend/api/shippers').then(function(res){
                        return res.data;
                    })
                }
            }
        }).
        when('/shipper/new', {
            templateUrl: '/partials/shipper-new.html',
            controller: 'ShipperNewController'
        }).
        when('/pickups', {
            templateUrl: '/partials/pickups.html',
            controller: 'PickupController'

        }).
        when('/pickup/new', {
            templateUrl: '/partials/pickup-new.html',
            controller: 'PickupNewController'
        }).

        when('/seals', {
            templateUrl: '/partials/seals.html',
            controller: 'SealController',
            resolve: {
                sealsList: function($http){
                    return $http.get('backend/api/seals').then(function(res){
                        return res.data;
                    });
                }
            }
        }).
        when('/seal/detail/:id', {
            templateUrl: '/partials/seal-detail.html',
            controller: 'SealDetailController',
            resolve: {
                Seal: function($http, $route){
                    return $http.get('backend/api/seal/'+$route.current.params.id).then(function(res){
                        return res.data;
                    })
                }
            }
        }).
        when('/shuttle/detail/:id', {
            templateUrl: '/partials/shuttle-detail.html',
            controller: 'ShuttleDetailController',
            resolve: {
                Shuttle: function($http, $route){
                    return $http.get('backend/api/cs/'+$route.current.params.id).then(function(res){
                        return res.data;
                    });
                }
            }
        }).
        when('/trucks', {
           templateUrl: '/partials/trucks.html',
            controller: 'TrucksController',
            resolve:{
                trucksList: function($http){
                    return $http.get('backend/api/trucks').then( function(res){
//                        console.log(res.data);
                        return res.data;
                    });
                }
            }
        }).
        when('/branches', {
            templateUrl: '/partials/branches.html',
            controller: 'BranchesController',
            resolve: {
                branchesList: function($http){
                    return $http.get('backend/api/branches').then(function(res){
                        return res.data;
                    })
                }
            }
        }).
        when('/history', {
            templateUrl: 'partials/shipment-history.html',
            controller: 'HistoryController',
            resolve: {
                shipmentHistoryList: function($http){
                    return $http.get('backend/api/history/shipment').then(function(res){
                        return res.data;
                    });
                }
            }
        }).
        when('/history/seal', {
            templateUrl: 'partials/seal-history.html',
            controller: 'SealHistoryController',
            resolve: {
                sealHistoryList: function($http){
                    return $http.get('backend/api/history/seal').then(function(res){
                        return res.data;
                    });
                }
            }
        }).
        when('/history/shuttle', {
            templateUrl: 'partials/shuttle-history.html',
            controller: 'ShuttleHistoryController',
            resolve: {
                shuttleHistoryList: function($http){
                    return $http.get('backend/api/history/shuttle').then(function(res){
                        return res.data;
                    });
                }
            }
        }).
        when('/shuttle-control', {
            templateUrl: '/partials/shuttle-control.html',
            controller: 'ShuttleController',
            resolve: {
                shuttleList: function($http){
                    return $http.get('backend/api/cs').then(function(res){
                        return res.data;
                    });
                }
            }
        }).
        when('/users', {
            templateUrl: '/partials/users.html',
            controller: 'UserController',
            resolve: {
                usersList: function($http){
                    return $http.get('backend/api/users').then(function(res){
                        return res.data;
                    })
                }
            },
            page: 'user'

        }).
        when('/warehouses', {
            templateUrl: '/partials/warehouses.html',
            controller: 'WarehousesController',
            resolve: {
                warehousesList: function($http){
                    return $http.get('backend/api/warehouses').then(function(res){
                        return res.data;
                    });
                },
                branchesList: function($http){
                    return $http.get('backend/api/branches').then(function(res){
                        return res.data;
                    })
                }
            }
        }).
        otherwise({
            redirectTo: '/login'
        });



//    interceptor for 401 error
    $httpProvider.interceptors.push(['$q', '$timeout', '$location', function($q, $timeout, $location){
        return {
            responseError: function(rejection){
                console.log('in responseError');
                if(rejection.status == 401){
                    console.log('dicovered 401');
                    $timeout(function(){
                        $location.path('/login');
                    }, 4000)
                    return $q.reject(rejection);
                }
            }
        }
    }])


}]);