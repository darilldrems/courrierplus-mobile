(function(){
    var courierPlusControllers = angular.module('courierPlusControllers', ['BackEndService', 'SessionService', 'MyConstants', 'MyFilters']);

//    login page controller
    courierPlusControllers.controller('LoginController', ['$scope', '$rootScope', '$location', 'AuthService', 'Session', function($scope, $rootScope, $location, AuthService, Session){

        $scope.credentials = {
            username: '',
            password: ''
        }

        $scope.message = '';


        $scope.currentUser = {};


        $scope.login = function(credentials){
            $scope.$broadcast('loading', {});
            AuthService.login(credentials).then(function(res){

                $scope.$broadcast('loading-done', {});

                if(res.error){
                    $scope.message = res.error.message;
                }else{
//                    raise login successful or display the username and password
//                    console.log('in login successful');
//                    console.log(Session);
                    $location.path('/dashboard');
                }
            }, function(){
//                do something else if it fails
                    $scope.message = 'Server is down please try again later.';
            })
        };
    }]);

//    start of dashboard controller
    courierPlusControllers.controller('DashboardController', ['$scope', '$location', 'Session', 'dashData', '$interval', 'Api', function($scope, $location, Session, dashData, $interval, Api){

        $scope.currentUser = Session;

        $scope.today = dashData.today;




        $scope.failed_deliveries = dashData.failed_deliveries;

        $scope.recent_deliveries = dashData.recent_deliveries;

        var shipment_by_status = angular.fromJson(dashData.shipments_by_status);
        console.log('byt status'+shipment_by_status);
        $scope.shipment_by_status = [];

        angular.forEach(shipment_by_status, function(val){
//            console.lg
            var sig = [val._id, val.total];
//            console.log('sig:'+sig);
            $scope.shipment_by_status.push(sig);
        })

//        console.log('status'+$scope.shipment_by_status);



        /*
        * this function will refresh the dashboard recent deliveries and failed deliveries
        * every 1 minute.
        * */
        $interval(function(){
            Api.dashboard().then(function(data){
                //trying to debug
                console.log('interval data: '+JSON.stringify(data));
                if(data.failed_deliveries)
                    $scope.failed_deliveries = data.failed_deliveries;

                if(data.recent_deliveries)
                    $scope.recent_deliveries = data.recent_deliveries;

                if(data.today){
                    console.log('yes today:'+data.today);
                    $scope.today = data.today;
                }

            })
        }, 30000)




        var data = google.visualization.arrayToDataTable([
            ['Day', 'Pickups', 'Delivery'],
            ['1,01,2015',  1000,      400],
            ['1,04,2015',  1170,      460],
            ['1,05,2015',  660,       1120],
            ['1,06,2015',  1030,      540]
        ]);
//        console.log(Session);


        var options = {
            title: 'Company Performance',
            curveType: 'function',
            legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('area-chart'));

        chart.draw(data, options);



        // Draw pie chart for shipments by status using google.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows($scope.shipment_by_status);

        // Set chart options
        var options = {'title':'Shipment  by Status',
            'width':400,
            'height':300};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('pie-chart'));
        chart.draw(data, options);


        $scope.hasRole = function(roles, role){
            if(roles.indexOf(role) > -1){
                return true;
            }else{
                return false;
            }
        }


    }]);

//    shipment controller
        courierPlusControllers.controller('ShipmentsController', ['$scope', '$rootScope', '$location', 'Session', 'shipmentLists', 'Api', 'shippersOption', 'statusOption', 'contentOption', function($scope, $rootScope, $location, Session, shipmentLists, Api, shippersOption, statusOption, contentOption){

            $scope.currentUser = Session;

            $scope.shipment_hawb = '';
            $scope.shipment_status = '';
            $scope.uploaded_on = '';
            $scope.shipment_content = '';
            $scope.shipment_shipper = '';

            $scope.filter_options = {
                shippers: shippersOption.shippers,
                content: contentOption.content_types,
                status: statusOption.status

            }

            console.log('content types'+$scope.filter_options.content);



            $scope.shipments = shipmentLists.shipments;

            $scope.isActive = function(page_name){
                if(page_name == 'shipments')
                    return true;
                return false;
            }

            $scope.currentPage = 1;
            $scope.total_per_page = 10;

            $scope.getList = function(page, model){
//                if(!options)
//                    options = {}
                $scope.$broadcast('loading', {});
                $scope.conditions = {
                    hawb: $scope.shipment_hawb,
                    shipper: $scope.shipment_shipper,
                    content: $scope.shipment_content,
                    status: $scope.shipment_status,
                    date: $scope.uploaded_on
                }
                console.log($scope.conditions);
                Api.lists(page, model, $scope.conditions).then(function(data){
                    $scope.$broadcast('loading-done', {});
                    if(data.shipments.length > 0){
                        console.log('done in lists');
                        console.log(data.shipments);
                        $scope.shipments = data.shipments;
                    }
                });
            }




            $scope.go = function(){
                console.log('in go');
                $scope.getList(1, 'shipments');
            }


            $scope.paginate = function(action){
                if($scope.shipments.length = $scope.total_per_page){
                    if(action == "next"){
                        $scope.currentPage = $scope.currentPage + 1;
                        $scope.getList($scope.currentPage, 'shipments');
                    }else{
                        $scope.currentPage = $scope.currentPage - 1;
                        $scope.getList($scope.currentPage-1, 'shipments');
                    }
                }

            }

            $scope.hasRole = function(roles, role){
                if(roles.indexOf(role) > -1){
                    return true;
                }else{
                    return false;
                }
            }


            $scope.export_action = '';

            $scope.exportAction = function(){
//            console.log('in here exportAction: '+$scope.export_action);
                switch($scope.export_action){
                    case 'pdf':
                        $scope.$broadcast('export-pdf', {});
                        console.log('event broadcasted');
                        break;
                    case 'excel':
                        $scope.$broadcast('export-excel', {});
                        break;
                    case 'doc':
                        $scope.$broadcast('export-doc', {});
                        break;
                    default:
                        console.log('no event caught');
                }

            }


        }]);

//    pickups controller
        courierPlusControllers.controller('PickupController', ['$scope', '$rootScope', '$location', function($scope, $rootScope, $location){

    }]);

    courierPlusControllers.controller('PickupNewController', ['$scope', '$rootScope', '$location', function($scope, $rootScope, $location){

    }]);

    courierPlusControllers.controller('SealController', ['$scope', '$rootScope', '$location', '$http', 'Api', 'sealsList', 'Location', 'Session', function($scope, $rootScope, $location, $http, Api, sealsList, Location, Session){

        $scope.currentUser = Session;

        $scope.message = '';

        $scope.hasRole = function(roles, role){
            if(roles.indexOf(role) > -1){
                return true;
            }else{
                return false;
            }
        }

        $scope.seal_filter = {
            number: '',
            date: ''
        }

        $scope.seal_hawbs = [];

        $scope.cities = Location.cities;



        $scope.seals = sealsList.seals;


        $scope.add = function(){
            $scope.new_seal.shipments.push($scope.hawb);
            $scope.hawb = "";
        }


        $scope.new_seal = {
            number: '',
            departure: '',
            destination: '',
            location: '',
            shipments: []
        }



        $scope.createSeal = function(){
            console.log($scope.new_seal.shipments.length);
            if($scope.sealForm.$valid && $scope.new_seal.shipments.length > 0 ){
                $scope.$broadcast('loading', {});
                console.log($scope.new_seal.destination + ' - ' + $scope.new_seal.departure);
                if($scope.new_seal.destination != $scope.new_seal.departure){
                    console.log('in here');
                    $scope.message = '';
                    $scope.new_seal.location = $scope.new_seal.departure;
                    Api.create('seal', $scope.new_seal).then(function(res){
                        $scope.new_seal = {
                            number: '',
                            departure: '',
                            destination: '',
                            location: '',
                            shipments: []
                        }

                        console.log('data: '+res);

                        Api.lists(1, 'seals').then(function(data){
                            $scope.$broadcast('loading-done', {});
                            $scope.seals = data.seals;
                        })
                    });
                }else{
                    $scope.message = 'Destination and Departure can not be the same';
                }
            }

        }


        $scope.isActive = function(page_name){
            if(page_name == 'seal')
                return true;
            return false;
        }


        $scope.currentPage = 1;
        $scope.total_per_page = 10;

        $scope.getList = function(page, model){
            Api.lists(page, model, $scope.seal_filter).then(function(data){
                if(data.seals.length > 0){
                    console.log('got inside here');
                    $scope.message = '';
                    $scope.seals = data.seals;
                }else{
                    console.log('got in no results');
                    $scope.message = 'no results found';
                }
            });
        }

        $scope.go = function(){
            $scope.getList(1, 'seals');
        }

        $scope.paginate = function(action){
            if($scope.seals.length = $scope.total_per_page){
                if(action == "next"){
                    $scope.currentPage = $scope.currentPage + 1;
                    $scope.getList($scope.currentPage, 'seals');
                }else{
                    $scope.currentPage = $scope.currentPage - 1;
                    $scope.getList($scope.currentPage, 'seals');
                }
            }

        }


        //        responsible for passing the value of the action selected to export the table
        $scope.export_action = '';

        $scope.exportAction = function(){
//            console.log('in here exportAction: '+$scope.export_action);
            switch($scope.export_action){
                case 'pdf':
                    $scope.$broadcast('export-pdf', {});
                    console.log('event broadcasted');
                    break;
                case 'excel':
                    $scope.$broadcast('export-excel', {});
                    break;
                case 'doc':
                    $scope.$broadcast('export-doc', {});
                    break;
                default:
                    console.log('no event caught');
            }

        }




    }]);

    courierPlusControllers.controller('SealDetailController', ['$scope', 'Session', 'Seal', function($scope, Session, Seal){
        $scope.currentUser = Session;

        $scope.seal = Seal.seal;

        $scope.hasRole = function(roles, role){
            if(roles.indexOf(role) > -1){
                return true;
            }else{
                return false;
            }
        }

        $scope.isActive = function(page_name){
            if(page_name == 'seal')
                return true;
            return false;
        }


        $scope.export_action = '';

        $scope.exportAction = function(){
//            console.log('in here exportAction: '+$scope.export_action);
            switch($scope.export_action){
                case 'pdf':
                    $scope.$broadcast('export-pdf', {});
                    console.log('event broadcasted');
                    break;
                case 'excel':
                    $scope.$broadcast('export-excel', {});
                    break;
                case 'doc':
                    $scope.$broadcast('export-doc', {});
                    break;
                default:
                    console.log('no event caught');
            }

        }


    }]);

    courierPlusControllers.controller('UserController', ['$scope', '$rootScope', '$location', 'Session', 'UserRoles','usersList', 'Api', function($scope, $rootScope, $location, Session, UserRoles, usersList, Api){
        $scope.currentUser = Session;
        $scope.user_roles = UserRoles.roles;

        $scope.isActive = function(page_name){
            if(page_name == 'users')
                return true;
            return false;
        }

        $scope.hasRole = function(roles, role){
            if(roles.indexOf(role) > -1){
                return true;
            }else{
                return false;
            }
        }

//        console.log(usersList);
        $scope.users = usersList.users;

        $scope.new_user = {
            username: '',
            email: '',
            first_name: '',
            role: '',
            last_name: '',
            password: ''
        }

        $scope.selected_item = 'default';

        $scope.message = ''
        $scope.createUser = function(){
            $scope.message = '';
            if($scope.userForm.$valid){
                $scope.$broadcast('loading', {});
                Api.register($scope.new_user).then(function(data){
//                    console.log(data);
                    if(data.user_id){
                        $scope.new_user = {
                            username: '',
                            email: '',
                            first_name: '',
                            role: '',
                            last_name: '',
                            password: ''
                        }
                        Api.listUsers(1).then(function(data){
                            $scope.$broadcast('loading-done', {});
                            $scope.users = data.users;
                        });
                    }else{
                        console.log(data);
                        $scope.$broadcast('loading-done', {});
                        if(data.error.code == 11000){
//                            console.log(data.error.code + typeof data.error.code);
                            $scope.message = 'Username already exist.'
                        }
                    }
                });
            }

        }
        $scope.action = "";

        $scope.itemSelected = function(e){
            console.log($scope.selected_item);
        }

        $scope.performAction = function(){
//            console.log($scope.action);
            if($scope.action == 'delete'){
//                console.log($scope.selected_item);
                $scope.$broadcast('loading', {});
                Api.delete('user', $scope.selected_item).then(function(data){
                    console.log(data.user);

                   if(data.user.state == 'deactivated'){
                       Api.listUsers(1).then(function(data){
                           $scope.$broadcast('loading-done', {});
                           $scope.users = data.users;
                       });
                   }else{
                       $scope.$broadcast('loading-done', {});
                   }
                });
            }
        }



        $scope.currentPage = 1;
        $scope.total_per_page = 10;

        $scope.getList = function(page, model){
            Api.lists(page, model).then(function(data){
                if(data.users.length > 0){
                    $scope.users = data.users;
                }
            });
        }

        $scope.paginate = function(action){
            if($scope.users.length = $scope.total_per_page){
                if(action == "next"){
                    $scope.currentPage = $scope.currentPage + 1;
                    $scope.getList($scope.currentPage, 'users');
                }else{
                    $scope.currentPage = $scope.currentPage - 1;
                    $scope.getList($scope.currentPage, 'users');
                }
            }

        }


    }]);

    courierPlusControllers.controller('ShipmentEditController', ['$scope', 'shipment', 'Api', 'Session', 'DeliveryStatus', '$location', function($scope, shipment, Api, Session, DeliveryStatus, $location){
        $scope.shipment = shipment.shipment;

        $scope.hasRole = function(roles, role){
            if(roles.indexOf(role) > -1){
                return true;
            }else{
                return false;
            }
        }

        $scope.delivery_statuses = DeliveryStatus.status

        $scope.currentUser = Session;

        $scope.isSelected = function(status, selected_status){
            if(status == selected_status)
                return true;
            return false;
        }

        $scope.editShipment = function(){
            $scope.$broadcast('loading', {});
            $scope.edited_shipment = {
                weight: $scope.shipment.weight,
                status: $scope.shipment.status,
                consignee_tel: $scope.shipment.consignee_tel,
                consignee_name: $scope.shipment.consignee_name,
                consignee_address: $scope.shipment.consignee_address
            }

            Api.update('shipment', $scope.shipment._id, $scope.edited_shipment).then(function(data){
                $scope.$broadcast('loading-done', {});
                if(!data.error){
                    $location.path('/shipment/detail/'+$scope.shipment.hawb);
                }else{
                    $scope.message = 'Some error occurred please try again later';
                }
            });

//            console.log($scope.edited_shipment.status);
        }



    }])

    courierPlusControllers.controller('ShipmentDetailController', ['$scope', '$location', '$routeParams', 'Api', 'Session', function($scope, $location, $routeParams, Api, Session){
        $scope.shipment_hawb = $routeParams.hawb;
        $scope.currentUser = Session;

        $scope.hasRole = function(roles, role){
            if(roles.indexOf(role) > -1){
                return true;
            }else{
                return false;
            }
        }

        Api.get('shipment', $scope.shipment_hawb).then(function(data){
            console.log('in here with data'+data);
            $scope.shipment = data.shipment;
        })


//        responsible for passing the value of the action selected to export the table
        $scope.export_action = '';

        $scope.exportAction = function(){
//            console.log('in here exportAction: '+$scope.export_action);
            switch($scope.export_action){
                case 'pdf':
                    $scope.$broadcast('export-pdf', {});
                    console.log('event broadcasted');
                    break;
                case 'excel':
                    $scope.$broadcast('export-excel', {});
                    break;
                case 'doc':
                    $scope.$broadcast('export-doc', {});
                    break;
                default:
                    console.log('no event caught');
            }

        }



    }]);

    courierPlusControllers.controller('ShipmentNewController', ['$scope', 'Session', 'Api', 'shippersList', 'warehouseList', '$location', 'DeliveryStatus', function($scope, Session, Api, shippersList, warehouseList, $location, DeliveryStatus){
        $scope.shippers = shippersList.shippers;

        $scope.currentUser = Session;

        $scope.hasRole = function(roles, role){
            if(roles.indexOf(role) > -1){
                return true;
            }else{
                return false;
            }
        }

        $scope.warehouses = warehouseList.warehouses;

        $scope.s_status = DeliveryStatus.status;

        $scope.shipment = {
            hawb: '',
            shipper: '',
            content: '',
            amount: '',
            status: '',
            consignee_name: '',
            consignee_tel: '',
            consignee_address: '',
            weight: '',
            pick_up_by: $scope.currentUser.id

        }

        $scope.createShipment = function(){
            if($scope.shipmentForm.$valid){
//                console.log('vaid'+$scope.shipment);
                $scope.$broadcast('loading', {});
                Api.create('shipment', $scope.shipment).then(function(data){
                    $scope.$broadcast('loading-done', {});
                    if(!data.error){
                        $location.path('/shipments');
                    }else{
                        console.log(data.error);
                        $scope.message = 'Error occurred please change details and refill';
                    }
                })
            }else{

                console.log($scope.shipmentForm.$error);
            }
        }

        $scope.isActive = function(page_name){
            if(page_name == 'shipments')
                return true;
            return false;
        }


    }])

    courierPlusControllers.controller('ShippersController', ['$scope', '$location', 'Api', 'Session', 'shippersList', function($scope, $location, Api, Session, shippersList){
        $scope.currentUser = Session;

        $scope.shippers = shippersList.shippers;

        $scope.filter__by_name = ''

        $scope.hasRole = function(roles, role){
//            console.log('roles: '+roles);
//            console.log(roles.indexOf(role));
//            console.log('current user role: '+
            console.log('currentUser role', $scope.currentUser.role);
            console.log('shipper role', +role);
            if(roles.indexOf(role) > -1){

                return true;
            }else{
                return false;
            }
        }

        $scope.isActive = function(page_name){
            if(page_name == 'shippers')
                return true;
            return false;
        }

        $scope.selected_item = ''


        $scope.performAction = function(){
//            console.log($scope.action);
            if($scope.action == 'delete'){
                $scope.$broadcast('loading', {});
//                console.log($scope.selected_item);
                Api.delete('shipper', $scope.selected_item).then(function(data){
                    console.log(data.shipper);
                    if(data.shipper.state == 'deactivated'){
                        Api.lists(1, 'shippers').then(function(data){
                            $scope.$broadcast('loading-done', {});
                            $scope.shippers = data.shippers;
                        });
                    }else{
                        $scope.$broadcast('loading-done', {});
                    }
                });
            }
        }

        $scope.currentPage = 1;
        $scope.total_per_page = 10;

        $scope.getList = function(page, model){
            Api.lists(page, model).then(function(data){
                if(data.shippers.length > 0){
                    $scope.shippers = data.shippers;
                }
            });
        }

        $scope.paginate = function(action){
            if($scope.shippers.length = $scope.total_per_page){
                if(action == "next"){
                    $scope.currentPage = $scope.currentPage + 1;
                    $scope.getList($scope.currentPage, 'shippers');
                }else{
                    $scope.currentPage = $scope.currentPage - 1;
                    $scope.getList($scope.currentPage, 'shippers');
                }
            }

        }

        //        responsible for passing the value of the action selected to export the table
        $scope.export_action = '';

        $scope.exportAction = function(){
//            console.log('in here exportAction: '+$scope.export_action);
            switch($scope.export_action){
                case 'pdf':
                    $scope.$broadcast('export-pdf', {});
                    console.log('event broadcasted');
                    break;
                case 'excel':
                    $scope.$broadcast('export-excel', {});
                    break;
                case 'doc':
                    $scope.$broadcast('export-doc', {});
                    break;
                default:
                    console.log('no event caught');
            }

        }


    }]);

    courierPlusControllers.controller('ShipperNewController', ['$scope', '$location', 'Session', 'Api', function($scope, $location, Session, Api){

        $scope.shipper_types = ['Credit', 'Non Credit'];
        $scope.currentUser = Session;

        $scope.message = ''

        $scope.hasRole = function(roles, role){
            console.log('shipper role', +role);
            if(roles.indexOf(role) > -1){
                return true;
            }else{
                return false;
            }
        }

        $scope.shipper = {
            name: '',
            address: '',
            tel: '',
            type: '',
            email: ''
        }

        $scope.isActive = function(page_name){
            if(page_name == 'shippers')
                return true;
            return false;
        }
        $scope.createShipper = function(){
            if($scope.shipperForm.$valid){
//                console.log('seen as valid');
                $scope.$broadcast('loading', {});
                console.log($scope.shipper);
                Api.create('shipper', $scope.shipper).then(function(res){
                    $scope.$broadcast('loading-done', {});
                    if(!res.error){
                        console.log('in no error');
                        $location.path('/shippers');

                    }else{
                        var error_m = data.error.err;
                        $scope.message = error_m.substring(error_m.indexOf('{')+4, error_m.indexOf('}'))+' is already in use. Please change it.';


                    }
                })
            }
        }
    }]);

    courierPlusControllers.controller('TrucksController', ['$scope', 'Session', 'Api', 'trucksList', function($scope, Session, Api, trucksList){
        $scope.isActive = function(page_name){
            if(page_name == 'trucks')
                return true;
            return false;
        }

        $scope.hasRole = function(roles, role){
            if(roles.indexOf(role) > -1){
                return true;
            }else{
                return false;
            }
        }

        $scope.message = '';

        $scope.action = ''

        $scope.trucks = trucksList.trucks;

        $scope.selected_item = '';

        $scope.currentUser = Session;

        $scope.truck = {
            name: '',
            identification: '',
            location: ''
        }

        $scope.createTruck = function(){
            if($scope.truckForm.$valid){
                $scope.message = '';
                $scope.$broadcast('loading', {});
                Api.create('truck', $scope.truck).then(function(data){
                    if(!data.error){
                        $scope.truck = {
                            name: '',
                            identification: '',
                            lacation: ''
                        }
                        Api.lists(1, 'trucks').then(function(data){
                            $scope.$broadcast('loading-done', {});
                            $scope.trucks = data.trucks;
                        });
                    }else{
                        $scope.$broadcast('loading-done', {});
                        var error_m = data.error.err;
                        $scope.message = error_m.substring(error_m.indexOf('{')+4, error_m.indexOf('}'))+' is already in use. Please change it.';
                    }
                })
            }
        }

        $scope.performAction = function(){
//            console.log($scope.action);
            if($scope.action == 'delete'){
//                console.log($scope.selected_item);
                console.log($scope.selected_item);
                $scope.$broadcast('loading', {});
                Api.delete('truck', $scope.selected_item).then(function(data){
//                    console.log()
                    console.log(data.user);
                    if(data.truck.state == 'deactivated'){
                        Api.lists(1, 'trucks').then(function(data){
                            $scope.$broadcast('loading-done', {});
                            $scope.trucks = data.trucks;
                        });
                    }else{
                        $scope.$broadcast('loading-done', {});
                    }
                });
            }
        }

        $scope.currentPage = 1;
        $scope.total_per_page = 10;

        $scope.getList = function(page, model){
            Api.lists(page, model).then(function(data){
                if(data.trucks.length > 0){
                    $scope.trucks = data.trucks;
                }
            });
        }

        $scope.paginate = function(action){
            if($scope.trucks.length = $scope.total_per_page){
                if(action == "next"){
                    $scope.currentPage = $scope.currentPage + 1;
                    $scope.getList($scope.currentPage, 'trucks');
                }else{
                    $scope.currentPage = $scope.currentPage - 1;
                    $scope.getList($scope.currentPage, 'trucks');
                }
            }

        }


    }]);

    courierPlusControllers.controller('BranchesController', ['$scope', 'branchesList', 'Api', 'Session', function($scope, branchesList, Api, Session){
        $scope.branches = branchesList.branches;

        $scope.currentUser = Session;

        $scope.action = '';

        $scope.branch = {
            name: '',
            location: ''
        }

        $scope.hasRole = function(roles, role){
            if(roles.indexOf(role) > -1){
                return true;
            }else{
                return false;
            }
        }

        $scope.message = '';

        $scope.selected_item = '';

        $scope.isActive = function(page_name){
            if(page_name == 'branches')
                return true
            return false;
        }

        $scope.createBranch = function(){
            if($scope.branchForm.$valid){
                console.log($scope.branch);
                $scope.$broadcast('loading', {});
                Api.create('branch', $scope.branch).then(function(data){
                    if(!data.error){
                        console.log('in successful');
                        $scope.branch = {
                            name: '',
                            location: ''
                        }
                        Api.lists(1, 'branches').then(function(data){
                            $scope.$broadcast('loading-done', {});
                            $scope.branches = data.branches;
                        });

                    }else{
                        $scope.$broadcast('loading-done', {});
                        var error_m = data.error.err;
                        $scope.message = error_m.substring(error_m.indexOf('{')+4, error_m.indexOf('}'))+' is already in use. Please change it.';

                    }
                });
            }
        }

        $scope.performAction = function(){
//            console.log($scope.action);
            if($scope.action == 'delete'){
//                console.log($scope.selected_item);
                $scope.$broadcast('loading', {});
                console.log($scope.selected_item);
                Api.delete('branch', $scope.selected_item).then(function(data){
//                    console.log()
//                    console.log(data.user);
                    console.log(data);
                    if(data.branch.state == 'deactivated'){
                        Api.lists(1, 'branches').then(function(data){
                            $scope.$broadcast('loading-done', {});
                            $scope.branches = data.branches;
                        });
                    }else{
                        $scope.$broadcast('loading-done', {});
                    }
                });
            }
        }


        $scope.currentPage = 1;
        $scope.total_per_page = 10;

        $scope.getList = function(page, model){
            Api.lists(page, model).then(function(data){
                if(data.branches.length > 0){
                    $scope.branches = data.branches;
                }
            });
        }

        $scope.paginate = function(action){
            if($scope.branches.length = $scope.total_per_page){
                if(action == "next"){
                    $scope.currentPage = $scope.currentPage + 1;
                    $scope.getList($scope.currentPage, 'branches');
                }else{
                    $scope.currentPage = $scope.currentPage - 1;
                    $scope.getList($scope.currentPage, 'branches');
                }
            }

        }



    }]);

    courierPlusControllers.controller('ShuttleController', ['$scope', 'shuttleList', 'Api', 'Session', 'Location', function($scope, shuttleList, Api, Session, Location){

        $scope.currentUser = Session;


        $scope.cities = Location.cities;

        $scope.message = ''

        console.log(shuttleList.cs);

        $scope.shuttles = shuttleList.cs;

        $scope.shuttle_filter = {
            number: '',
            date: ''
        }


        $scope.add = function(){
            $scope.shuttle.shipments.push($scope.hawb);
            $scope.hawb = "";
        }


        $scope.shuttle = {
            number: '',
            departure: '',
            destination: '',
            location: '',
            shipments: []
        }

        $scope.isActive = function(page_name){
            if(page_name == 'shuttle')
                return true;
            return false;
        }



        $scope.createShuttle = function(){
//            console.log($scope.new_seal.shipments.length);
            if($scope.shuttleForm.$valid && $scope.shuttle.shipments.length > 0 ){
                $scope.$broadcast('loading', {});
                console.log($scope.shuttle.destination + ' - ' + $scope.shuttle.departure);
                if($scope.shuttle.destination != $scope.shuttle.departure){
//                    console.log('in here');
                    $scope.message = '';
                    $scope.shuttle.location = $scope.shuttle.departure;
                    Api.create('cs', $scope.shuttle).then(function(res){
                        $scope.shuttle = {
                            number: '',
                            departure: '',
                            destination: '',
                            location: '',
                            shipments: []
                        }

                        console.log('data: '+res);

                        Api.lists(1, 'cs').then(function(data){
                            $scope.$broadcast('loading-done', {});
                            console.log('gere: '+data);
                            $scope.shuttles = data.cs;
                        })
                    });
                }else{
                    $scope.message = 'Destination and Departure can not be the same';
                }
            }

        }



        $scope.currentPage = 1;
        $scope.total_per_page = 10;

        $scope.go = function(){
            $scope.getList(1,'cs');
        }

        $scope.getList = function(page, model){
            Api.lists(page, model, $scope.shuttle_filter).then(function(data){
                if(data.cs.length > 0){
                    $scope.shuttles = data.cs;
                }else{
                    $scope.message = 'no result found';
                }
            });
        }

        $scope.paginate = function(action){
            if($scope.shuttles.length = $scope.total_per_page){
                if(action == "next"){
                    $scope.currentPage = $scope.currentPage + 1;
                    $scope.getList($scope.currentPage, 'cs');
                }else{
                    $scope.currentPage = $scope.currentPage - 1;
                    $scope.getList($scope.currentPage, 'cs');
                }
            }

        }


        //        responsible for passing the value of the action selected to export the table
        $scope.export_action = '';

        $scope.exportAction = function(){
//            console.log('in here exportAction: '+$scope.export_action);
            switch($scope.export_action){
                case 'pdf':
                    $scope.$broadcast('export-pdf', {});
                    console.log('event broadcasted');
                    break;
                case 'excel':
                    $scope.$broadcast('export-excel', {});
                    break;
                case 'doc':
                    $scope.$broadcast('export-doc', {});
                    break;
                default:
                    console.log('no event caught');
            }

        }

        $scope.hasRole = function(roles, role){
            if(roles.indexOf(role) > -1){
                return true;
            }else{
                return false;
            }
        }




    }]);

    courierPlusControllers.controller('ShuttleDetailController', ['$scope', 'Session', 'Shuttle', function($scope, Session, Shuttle){
        $scope.currentUser = Session;

        $scope.shuttle = Shuttle.cs;

        $scope.isActive = function(page_name){
            if(page_name == 'shuttle')
                return true;
            return false;
        }

        $scope.hasRole = function(roles, role){
            if(roles.indexOf(role) > -1){
                return true;
            }else{
                return false;
            }
        }


        $scope.export_action = '';

        $scope.exportAction = function(){
//            console.log('in here exportAction: '+$scope.export_action);
            switch($scope.export_action){
                case 'pdf':
                    $scope.$broadcast('export-pdf', {});
                    console.log('event broadcasted');
                    break;
                case 'excel':
                    $scope.$broadcast('export-excel', {});
                    break;
                case 'doc':
                    $scope.$broadcast('export-doc', {});
                    break;
                default:
                    console.log('no event caught');
            }

        }



    }])


    courierPlusControllers.controller('WarehousesController', ['$scope', 'Api', 'Session', 'warehousesList', 'branchesList', function($scope, Api, Session, warehousesList, branchesList){
        $scope.warehouses = warehousesList.warehouses;

        $scope.currentUser = Session;

        $scope.branches = branchesList.branches;

        $scope.warehouse = {
            name: '',
            branch: ''
        }

        $scope.isActive = function(page_name){
            if(page_name == 'warehouses')
                return true;
            return false;
        }

        $scope.action = '';

        $scope.hasRole = function(roles, role){
            if(roles.indexOf(role) > -1){
                return true;
            }else{
                return false;
            }
        }

        $scope.createWarehouse = function(){
            if($scope.warehouseForm.$valid){
                console.log('warehouse new'+$scope.warehouse);
                $scope.$broadcast('loading', {});
                Api.create('warehouse', $scope.warehouse).then(function(data){
                    if(!data.error){
                        $scope.warehouse= {
                            name:'',
                            branch: ''
                        }
                        Api.lists(1, 'warehouses').then(function(data){
                            $scope.$broadcast('loading-done', {});
                            $scope.warehouses = data.warehouses;
                        })

                    }else{
                        $scope.$broadcast('loading-done', {});
                        console.log(data.error);
                    }
                })
            }
        }

        $scope.performAction = function(){
            console.log($scope.action);
            if($scope.action == 'delete'){
//                console.log($scope.selected_item);
//                console.log($scope.selected_item);
                $scope.$broadcast('loading', {});
                Api.delete('warehouse', $scope.selected_item).then(function(data){
//                    console.log()
                    console.log(data.warehouse);
                    if(data.warehouse.state == 'deactivated'){
                        Api.lists(1, 'warehouses').then(function(data){
                            $scope.$broadcast('loading-done', {});
                            $scope.warehouses = data.warehouses;
                        });
                    }else{
                        $scope.$broadcast('loading-done', {});
                    }
                });
            }
        }



        $scope.currentPage = 1;
        $scope.total_per_page = 10;

        $scope.getList = function(page, model){
            Api.lists(page, model).then(function(data){
                if(data.warehouses.length > 0){
                    $scope.warehouses = data.warehouses;
                }
            });
        }

        $scope.paginate = function(action){
            if($scope.warehouses.length = $scope.total_per_page){
                if(action == "next"){
                    $scope.currentPage = $scope.currentPage + 1;
                    $scope.getList($scope.currentPage, 'warehouses');
                }else{
                    $scope.currentPage = $scope.currentPage - 1;
                    $scope.getList($scope.currentPage, 'warehouses');
                }
            }

        }
    }])


    courierPlusControllers.controller('HistoryController', ['$scope', 'Api', 'Session', 'shipmentHistoryList', function($scope, Api, Session, shipmentHistoryList){

        $scope.currentUser = Session;

        $scope.shipment_history = shipmentHistoryList.results;

        $scope.currentPage = 1;
        $scope.total_per_page = 10;

        $scope.filter = {
            page: 1,
            date: '',
            hawb: ''
        }

        $scope.getList = function(model_name, filter){
            $scope.$broadcast('loading', {});
            Api.history(model, filter).then(function(data){
                $scope.$broadcast('loading-done', {});
                if(data.results.length > 0){
                    $scope.shipment_history = data.results;
                }
            })

        }

        $scope.paginate = function(action){
            if($scope.shipment_history.length = $scope.total_per_page){


                if(action == "next"){
                    $scope.currentPage = $scope.currentPage + 1;
                    $scope.filter.page = $scope.currentPage;
                    $scope.getList('shipment', $scope.filter);
                }else{
                    $scope.currentPage = $scope.currentPage - 1;
                    $scope.filter.page = $scope.currentPage;
                    $scope.getList('shipment', $scope.filter);
                }
            }

        }


        $scope.filterHistory = function(){
            console.log($scope.filter.date);
            $scope.$broadcast('loading', {});
            Api.history('shipment', $scope.filter).then(function(data){
                $scope.$broadcast('loading-done', {});
                if(data.results){
                    $scope.shipment_history = data.results;
                    if(data.results.length <= 0){
                        $scope.message = 'No result found';
                    }
                }else{
                    $scope.message = 'try again later some error occured';
                }
            })
        }

        $scope.hasRole = function(roles, role){
            if(roles.indexOf(role) > -1){
                return true;
            }else{
                return false;
            }
        }



        $scope.export_action = '';

        $scope.exportAction = function(){
//            console.log('in here exportAction: '+$scope.export_action);
            switch($scope.export_action){
                case 'pdf':
                    $scope.$broadcast('export-pdf', {});
                    console.log('event broadcasted');
                    break;
                case 'excel':
                    $scope.$broadcast('export-excel', {});
                    break;
                case 'doc':
                    $scope.$broadcast('export-doc', {});
                    break;
                default:
                    console.log('no event caught');
            }

        }


    }])


    courierPlusControllers.controller('SealHistoryController', ['$scope', 'Session', 'Api', 'sealHistoryList', function($scope, Session, Api, sealHistoryList){

        $scope.currentUser = Session;

        $scope.seal_history = sealHistoryList.results;

        $scope.filter = {
            date: '',
            number: '',
            page: 1
        }



        $scope.export_action = '';

        $scope.exportAction = function(){
//            console.log('in here exportAction: '+$scope.export_action);
            switch($scope.export_action){
                case 'pdf':
                    $scope.$broadcast('export-pdf', {});
                    console.log('event broadcasted');
                    break;
                case 'excel':
                    $scope.$broadcast('export-excel', {});
                    break;
                case 'doc':
                    $scope.$broadcast('export-doc', {});
                    break;
                default:
                    console.log('no event caught');
            }

        }


        $scope.filterHistory = function(){
            console.log($scope.filter.date);
            $scope.$braodcast('loading', {});
            Api.history('seal', $scope.filter).then(function(data){
                $scope.$broadcast('loading-done', {});
                if(data.results){
                    $scope.seal_history = data.results;
                    if(data.results.length <= 0){
                        $scope.message = 'No result found';
                    }
                }else{
                    $scope.message = 'try again later some error occured';
                }
            })
        }


        $scope.currentPage = 1;
        $scope.total_per_page = 10;


        $scope.getList = function(model_name, filter){
            $scope.$broadcast('loading', {});
            Api.history(model, filter).then(function(data){
                $scope.$broadcast('loading-done', {});
                if(data.results.length > 0){
                    $scope.seal_history = data.results;
                }
            })

        }

        $scope.paginate = function(action){
            if($scope.seal_history.length = $scope.total_per_page){


                if(action == "next"){
                    $scope.currentPage = $scope.currentPage + 1;
                    $scope.filter.page = $scope.currentPage;
                    $scope.getList('seal', $scope.filter);
                }else{
                    $scope.currentPage = $scope.currentPage - 1;
                    $scope.filter.page = $scope.currentPage;
                    $scope.getList('seal', $scope.filter);
                }
            }

        }

        $scope.hasRole = function(roles, role){
            if(roles.indexOf(role) > -1){
                return true;
            }else{
                return false;
            }
        }



    }]);


    courierPlusControllers.controller('ShuttleHistoryController', ['$scope', 'Session', 'Api', 'shuttleHistoryList', function($scope, Session, Api, shuttleHistoryList){

        $scope.currentUser = Session;

        $scope.shuttle_history = shuttleHistoryList.results;

        $scope.filter = {
            date: '',
            number: '',
            page: 1
        }



        $scope.export_action = '';

        $scope.exportAction = function(){
//            console.log('in here exportAction: '+$scope.export_action);
            switch($scope.export_action){
                case 'pdf':
                    $scope.$broadcast('export-pdf', {});
                    console.log('event broadcasted');
                    break;
                case 'excel':
                    $scope.$broadcast('export-excel', {});
                    break;
                case 'doc':
                    $scope.$broadcast('export-doc', {});
                    break;
                default:
                    console.log('no event caught');
            }

        }


        $scope.filterHistory = function(){
            console.log($scope.filter.date);
            $scope.$broadcast('loading', {});
            Api.history('shuttle', $scope.filter).then(function(data){
                $scope.$broadcast('loading-done', {});
                if(data.results){
                    $scope.shuttle_history = data.results;
                    if(data.results.length <= 0){
                        $scope.message = 'No result found';
                    }
                }else{
                    $scope.message = 'try again later some error occured';
                }
            })
        }


        $scope.currentPage = 1;
        $scope.total_per_page = 10;


        $scope.getList = function(model_name, filter){
            $scope.$broadcast('loading', {});
            Api.history(model, filter).then(function(data){
                $scope.$broadcast('loading-done', {});
                if(data.results.length > 0){
                    $scope.shuttle_history = data.results;
                }
            })

        }

        $scope.paginate = function(action){
            if($scope.shuttle_history.length = $scope.total_per_page){
                if(action == "next"){
                    $scope.currentPage = $scope.currentPage + 1;
                    $scope.filter.page = $scope.currentPage;
                    $scope.getList('seal', $scope.filter);
                }else{
                    $scope.currentPage = $scope.currentPage - 1;
                    $scope.filter.page = $scope.currentPage;
                    $scope.getList('seal', $scope.filter);
                }
            }

        }

        $scope.hasRole = function(roles, role){
            if(roles.indexOf(role) > -1){
                return true;
            }else{
                return false;
            }
        }



    }])





})();